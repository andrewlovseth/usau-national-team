<?php

/*

	Template Name: Schedule & Results
	Template Post Type: page, world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>

  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<article>
	
				<?php if(have_rows('schedule')): ?>

					<section id="schedule">

						<h2>Schedule</h2>

						<?php while(have_rows('schedule')) : the_row(); ?>

						    <?php if( get_row_layout() == 'game_block' ): ?>
								
								<div class="game-block">
						    		<h4><?php the_sub_field('headline'); ?></h4>

						    		<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
									 
									  	<?php get_template_part('partials/fixture'); ?>

									<?php endwhile; endif; ?>
								</div>

						    <?php endif; ?>

						<?php endwhile; ?>
					
					</section>

				<?php endif; ?>


				<?php if(have_rows('results')): ?>

					<section id="results">

						<h2>Results</h2>

						<?php while(have_rows('results')) : the_row(); ?>

						    <?php if( get_row_layout() == 'game_block' ): ?>
								
								<div class="game-block">
						    		<h4><?php the_sub_field('headline'); ?></h4>

						    		<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
									 
									  	<?php get_template_part('partials/result'); ?>

									<?php endwhile; endif; ?>
								</div>

						    <?php endif; ?>

						<?php endwhile; ?>
					
					</section>

				<?php endif; ?>

			</article>

			<div class="aside-wrapper">
				
				<aside id="stats">
					<h3>Stats</h3>

					<table class="tablesorter">
						<thead> 
							<tr> 
							    <th class="player">Player</th> 
							    <th class="assists">Assists</th> 
							    <th class="goals">Goals</th> 
							    <th class="total">Total</th> 
							</tr> 
						</thead> 
						<tbody>
							
							<?php if(have_rows('stats')): while(have_rows('stats')): the_row(); ?>
						        
								<tr> 
								    <td class="player">
										<?php $post_object = get_sub_field('player'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										<?php wp_reset_postdata(); endif; ?>
								    </td> 
								    <td class="assists"><?php the_sub_field('assists'); ?></td> 
								    <td class="goals"><?php the_sub_field('goals'); ?></td> 
								    <td class="total"><?php the_sub_field('total'); ?></td> 
								</tr> 

							<?php endwhile; endif; ?>

						</tbody> 
					</table>

				</aside>

			</div>

		</div>
	</section>




<?php get_footer(); ?>