<?php

/*

	Template Name: History
	Template Post Type: world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>


	<section id="main">
		<div class="wrapper">

			<section id="history">

				<?php if(have_rows('teams')): while(have_rows('teams')): the_row(); ?>

				 	<div class="team">

				 		<div class="header">
				 			<div class="basic-info">
					        	<h2><?php the_sub_field('year'); ?></h2>
					        	<h3><?php the_sub_field('location'); ?></h3>
					        </div>

				        	<div class="finish">
				        		<span<?php if(get_sub_field('medal')) { echo ' class="medal ' . get_sub_field('medal') .'"'; } ?>><?php the_sub_field('finish'); ?></span>
				        	</div>
				 		</div>

				    	<div class="photo">
				        	<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />


							<?php if(have_rows('results')): ?>

								<section id="results">

									<h2>Results</h2>

									<?php while(have_rows('results')) : the_row(); ?>

									    <?php if( get_row_layout() == 'game_block' ): ?>
											
											<div class="game-block">
									    		<h4><?php the_sub_field('headline'); ?></h4>

									    		<?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
												 
												  	<?php get_template_part('partials/result'); ?>

												<?php endwhile; endif; ?>
											</div>

									    <?php endif; ?>

									<?php endwhile; ?>
								
								</section>

							<?php endif; ?>

				   		</div>

				   		<div class="info">

				   			<?php get_template_part('partials/roster'); ?>

				   		</div>
				    </div>

				<?php endwhile; endif; ?>
				

			</section>

		</div>
	</section>

<?php get_footer(); ?>