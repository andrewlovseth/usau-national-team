<?php get_header(); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php if(get_field('jersey_number')): ?>#<?php the_field('jersey_number'); ?> / <?php endif; ?><?php the_field('position'); ?></span>
				</h2>
				<h1>
					<span><?php the_title(); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article>

				<div id="tab-links">

					<a href="#bio">Bio</a>
					<a href="#playing-history">Playing History</a>
					<a href="#career-highlights">Career Highlights</a>
				</div>

				<div id="tabs">

					<div id="bio">
						<?php the_field('bio'); ?>
					</div>

					<div id="playing-history">
						<?php if(have_rows('playing_history')): while(have_rows('playing_history')): the_row(); ?>
 
						    <div class="item">
						        <span class="year key"><?php the_sub_field('years'); ?></span>
						        <span class="team value"><?php the_sub_field('team'); ?></span>
						    </div>

						<?php endwhile; endif; ?>
					</div>

					<div id="career-highlights">
						<?php if(have_rows('career_highlights')): while(have_rows('career_highlights')): the_row(); ?>
 
						    <div class="item">
						        <span class="year key"><?php the_sub_field('year'); ?></span>
						        <span class="highlight value"><?php the_sub_field('highlight'); ?></span>
						    </div>

						<?php endwhile; endif; ?>
					</div>

				</div>

			</article>

			<div class="aside-wrapper">

				<aside id="vitals">

					<?php if(get_field('place_of_birth')): ?>
						<h3>Place of Birth</h3>
						<p><?php the_field('place_of_birth'); ?></p>
					<?php endif; ?>

					<?php if(get_field('hometown')): ?>
						<h3>Hometown</h3>
						<p><?php the_field('hometown'); ?></p>
					<?php endif; ?>

					<?php if(get_field('current_residence')): ?>
						<h3>Current Residence</h3>
						<p><?php the_field('current_residence'); ?></p>
					<?php endif; ?>

					<?php if(get_field('birthdate')): ?>
						<h3>Birthdate</h3>
						<p><?php the_field('birthdate'); ?></p>
					<?php endif; ?>

					<?php if(get_field('club')): ?>
						<h3>Club</h3>
						<p><?php the_field('club'); ?></p>
					<?php endif; ?>

					<?php if(get_field('college')): ?>
						<h3>College</h3>
						<p><?php the_field('college'); ?></p>
					<?php endif; ?>

				</aside>

			</div>

		</div>
	</section>

<?php get_footer(); ?>