<?php

/*

	Template Name: History Index
	Template Post Type: world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>


	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php $event = get_post_type(get_the_ID()); the_field($event . '_event_name', 'options'); ?></span>
				</h2>
				<h1>
					<span>History</span>
				</h1>
			</div>

		</div>
	</section>


	<section id="main">
		<div class="wrapper">

			<?php if(have_rows('archive')): while(have_rows('archive')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'divsion' ): ?>
					
					<div class="division">
			    		<h3><?php the_sub_field('division_headline'); ?></h3>

			    		<?php if(have_rows('results')): ?>

				    		<div class="results">
				    			<div class="row row-header">
				    				<div class="year">
				    					<strong>Year</strong>
				    				</div>

				    				<div class="location">
				    					<strong>Location</strong>
				    				</div>

				    				<div class="record">
				    					<strong>Record</strong>
				    				</div>

				    				<div class="finish">
				    					<strong>Finish</strong>
				    				</div>

				    				<div class="details">
				    					<strong>Details</strong>
				    				</div>				    				
				    			</div>

					    		<?php while(have_rows('results')): the_row(); ?>
							    	<?php $team = get_sub_field('team'); ?>
							 
								    <div class="row">
								    	<div class="year">
								    		<p><?php the_field('year', $team->ID); ?></p>
								    	</div>

								    	<div class="location">
								    		<p><?php the_field('location', $team->ID); ?></p>
								    	</div>	

					    				<div class="record">
					    					<p><?php the_field('record', $team->ID); ?></p>
					    				</div>

							        	<div class="finish">
							        		<p><span<?php if(get_field('medal', $team->ID)) { echo ' class="medal ' . get_field('medal', $team->ID) .'"'; } ?>><?php the_field('finish', $team->ID); ?></span></p>
							        	</div>

							        	<div class="details">
							        		<p><a href="<?php echo get_permalink($team->ID); ?>">View</a></p>
							        	</div>

								    </div>

								<?php endwhile; ?>			    			

				    		</div>

						<?php endif; ?>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>


		</div>
	</section>

<?php get_footer(); ?>