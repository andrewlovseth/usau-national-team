<?php get_header(); ?>


	<?php $event = get_post_type(get_the_ID()); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field($event . '_hero_image', 'options'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span><?php the_field($event . '_event_dates', 'options'); ?> | <?php the_field($event . '_event_location', 'options'); ?></span>
				</h2>
				<h1>
					<span><?php the_field($event . '_event_name', 'options'); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<article>
				<div class="about">
					<?php the_field($event . '_about_info', 'options'); ?>
				</div>


				<?php if(have_rows($event . '_faqs', 'options')): ?>

					<div class="faqs">
						<h2>FAQs</h2>

						<?php while(have_rows($event . '_faqs', 'options')): the_row(); ?>

							<div class="faq">
								<a href="#" class="question"><?php the_sub_field('question'); ?></a>
								<div class="answer">
							        <?php the_sub_field('answer'); ?>
							    </div>
							</div>

						<?php endwhile; ?>
					</div>
	
				<?php endif; ?>

			</article>


			<div class="aside-wrapper">

				<?php
					$cat = get_post_type(get_the_ID());
					if($cat == 'world_games') {
						$cat = 'world-games';
					}

					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 4,
						'category_name' => $cat
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : ?>

					<aside id="latest-news">
						<h3>Latest News</h3>

						<?php while ( $query->have_posts() ) : $query->the_post(); ?>

							<?php get_template_part('partials/quick-news-article'); ?>

						<?php endwhile; ?>

						<a href="<?php echo site_url('/' . $cat . '/news/'); ?>" class="btn">All News</a>

					</aside>

				<?php endif; wp_reset_postdata(); ?>


				<?php if(have_rows($event . '_quick_schedule', 'options')): ?>

					<aside id="quick-schedule">
						<h3>Schedule</h3>
						
						<?php if(get_field($event . '_quick_schedule_headline', 'options')): ?>
							<h4><?php the_field($event . '_quick_schedule_headline', 'options'); ?></h4>
						<?php endif; ?>

						<?php while(have_rows($event . '_quick_schedule', 'options')): the_row(); ?>
						 

							<div class="fixture">


								<div class="match-up">
									<?php if(get_sub_field('non_game')): ?>
										
										<span class="non-game"><?php the_sub_field('non_game_headline'); ?></span>

									<?php else: ?>

										<span class="usa"><?php the_sub_field('usa_team'); ?></span>
										<span class="versus">v</span>
										<span class="opponent"><?php the_sub_field('opponent'); ?></span>

									<?php endif; ?>
							    </div>


								<div class="time">

									<?php if(get_sub_field('non_game')): ?>
										
										<?php if(get_sub_field('time') !== ''): ?>
											<?php the_sub_field('time'); ?>
										<?php endif; ?>

									<?php else: ?>

										<?php if(get_sub_field('round') !== ''): ?>
											<?php the_sub_field('round'); ?>
										<?php endif; ?>

										<?php if(get_sub_field('time') !== ''): ?>
											- <?php the_sub_field('time'); ?>
										<?php endif; ?>

									<?php endif; ?>


								</div>

							</div>

						<?php endwhile; ?>

					</aside>

				<?php endif; ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>