<?php

/*

	Template Name: Support the Team
	Template Post Type: page, world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<article class="default">
				<?php the_field('content'); ?>
			</article>

			<div class="aside-wrapper">

				<?php if(get_field('twitter') || get_field('instagram') || get_field('facebook') ): ?>

					<aside id="social">

						<h3>Follow the team</h3>

						<?php if(get_field('twitter')): ?>
							<a href="<?php the_field('twitter'); ?>" class="ir social twitter" rel="external">Twitter</a>
						<?php endif; ?>

						<?php if(get_field('instagram')): ?>
							<a href="<?php the_field('instagram'); ?>" class="ir social instagram" rel="external">Instagram</a>
						<?php endif; ?>

						<?php if(get_field('facebook')): ?>
							<a href="<?php the_field('facebook'); ?>" class="ir social facebook" rel="external">Facebook</a>
						<?php endif; ?>
					</aside>

				<?php endif; ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>