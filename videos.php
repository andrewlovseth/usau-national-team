<?php

/*

	Template Name: Videos
	Template Post Type: page, world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>

  	<?php get_template_part('partials/hero'); ?>

	<?php $posts = get_field('videos'); $max = 1; $i = 0; if( $posts ): ?>

		<section id="video-player">
			<div class="wrapper">

				<?php foreach( $posts as $post): $i++; ?>

					<?php if( $i > $max){ break; } setup_postdata($post); ?>

					<div id="video-content">

						<div class="player">

				        	<?php if(have_rows('video_id')): while(have_rows('video_id')) : the_row(); ?>

							    <?php if( get_row_layout() == 'youtube' ): ?>
						        	<iframe src="http://www.youtube-nocookie.com/embed/<?php the_sub_field('id'); ?>?rel=0&showinfo=0&modestbranding=1&vq=hd1080&autoplay=0" frameborder="0" width="1920" height="1080" allowfullscreen></iframe>
							    <?php endif; ?>

							    <?php if( get_row_layout() == 'vimeo' ): ?>
							    	<iframe src="http://player.vimeo.com/video/<?php the_sub_field('id'); ?>?title=0&color=a30034&autoplay=1&byline=0" frameborder="0" width="1920" height="1080"></iframe>      
							    <?php endif; ?>
							 
							<?php endwhile; endif; ?>

						</div>
			    
			    		<div class="info">
				   	    	<h3><?php the_title(); ?></h3>
			        		<?php the_field('description'); ?>
			        	</div>

			        </div>

				<?php endforeach; ?>

			</div>
		</section>


		<section id="main">
			<div class="wrapper">

				<section id="video-gallery">

				    <?php foreach( $posts as $post): ?>

				        <div class="thumbnail">

				        	<?php if(have_rows('video_id')): while(have_rows('video_id')) : the_row(); ?>

							    <?php if( get_row_layout() == 'youtube' ): ?>
							    		
						        	<a href="#" data-video-src="<?php echo get_permalink();?>" data-video-type="youtube">
						        		<img src="<?php $image = get_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						        		<span><?php the_title(); ?></span>
						        	</a>

							    <?php endif; ?>

							    <?php if( get_row_layout() == 'vimeo' ): ?>
							    		
						        	<a href="#" data-video-src="<?php echo get_permalink();?>" data-video-type="vimeo">
						        		<img src="<?php $image = get_field('thumbnail'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						        		<span><?php the_title(); ?></span>
						        	</a>

							    <?php endif; ?>
							 
							<?php endwhile; endif; ?>
			        
				        </div>

				    <?php endforeach; ?>

				</section>

			</div>
		</section>

	<?php wp_reset_postdata(); endif; ?>

<?php get_footer(); ?>