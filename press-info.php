<?php

/*

	Template Name: Press Info
	Template Post Type: page, world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>


  	<?php get_template_part('partials/hero'); ?>


	<section id="main">
		<div class="wrapper">

			<article class="default">
				<?php the_field('content'); ?>
			</article>

			<div class="aside-wrapper">

				<aside id="ctas">
					<?php if(have_rows('buttons')): while(have_rows('buttons')) : the_row(); ?>
 
					    <?php if( get_row_layout() == 'red_button' ): ?>
							
							<div class="cta">
					    		<a href="<?php the_sub_field('link'); ?>" class="btn red"><?php the_sub_field('label'); ?></a>
							</div>
							
					    <?php endif; ?>

					    <?php if( get_row_layout() == 'blue_button' ): ?>
							
							<div class="cta">
					    		<a href="<?php the_sub_field('link'); ?>" class="btn blue"><?php the_sub_field('label'); ?></a>
							</div>
							
					    <?php endif; ?>
					 
					<?php endwhile; endif; ?>

				</aside>

			</div>

		</div>
	</section>

<?php get_footer(); ?>