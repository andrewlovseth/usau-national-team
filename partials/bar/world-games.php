<?php if( is_singular('world_games') || is_post_type_archive('world_games') || (is_singular('players') && has_category('world-games')) ): ?>

	<nav id="bar">
		<div class="wrapper">

			<h1><a href="<?php the_field('world_games_event_homepage', 'options'); ?>"><?php the_field('world_games_event_name', 'options'); ?></a></h1>

			<?php $posts = get_field('world_games_event_nav_bar', 'options'); if( $posts !== null ): ?>

				<a href="#" id="menu-toggle">Menu</a>

				<div class="menu">
					<?php get_template_part('partials/bar/menu'); ?>
				</div>

			<?php wp_reset_postdata(); endif; ?>

		</div>
	</nav>

<?php endif; ?>