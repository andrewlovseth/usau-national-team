<?php foreach( $posts as $post): setup_postdata($post); ?>

    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

<?php endforeach; ?>