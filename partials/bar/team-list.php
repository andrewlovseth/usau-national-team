
<?php
	$altLabel = get_sub_field('alt_label');
	$divider = get_sub_field('divider');
?>

<?php if($divider == true): ?>

	

<?php else: ?>

	<?php $post_object = get_sub_field('link'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

		<?php if($altLabel): ?>

			<a href="<?php the_permalink(); ?>"><?php echo $altLabel; ?></a>

		<?php else: ?>

			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

		<?php endif; ?>

	<?php wp_reset_postdata(); endif; ?>

<?php endif; ?>