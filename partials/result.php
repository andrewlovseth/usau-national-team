<div class="result">

	<div class="time">
		<?php the_sub_field('time'); ?>
	</div>

	<div class="match-up">
		<span class="usa">USA</span>
		<span class="versus"><?php the_sub_field('usa_score'); ?> - <?php the_sub_field('opponent_score'); ?></span>
		<span class="opponent"><?php the_sub_field('opponent'); ?></span>
    </div>

    <?php if(get_sub_field('Link')): ?>
		<div class="link">
			<a href="<?php the_sub_field('Link'); ?>"><?php the_sub_field('link_label'); ?></a>
		</div>
	<?php endif; ?>

</div>