<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
	<div class="wrapper">

		<div class="info">
			<h2>
				<span><?php $event = get_post_type(get_the_ID()); the_field($event . '_event_name', 'options'); ?></span>
			</h2>
			<h1>
				<span><?php the_title(); ?></span>
			</h1>
		</div>

	</div>
</section>