<div id="roster">

    <?php if(have_rows('roster')): ?>
        <div class="roster">
        	<h4>Roster</h4>

            <div class="grid">
            	<?php while(have_rows('roster')): the_row(); ?>
             
                    <div class="player">
                        <div class="photo">
                            <img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>

                        <div class="info">
                        	<span class="name"><?php the_sub_field('name'); ?></span>
                        	<span class="city"><?php the_sub_field('city'); ?></span>
                        </div>
                    </div>

        	   <?php endwhile; ?>                    
            </div>
        </div>
    <?php endif; ?>


    <?php if(have_rows('alternates')): ?>
        <div class="alternates">
        	<h4>Alternates</h4>

            <div class="grid">
        	<?php while(have_rows('alternates')): the_row(); ?>
                
                    <div class="player">
                        <div class="photo">
                            <img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>

                        <div class="info">
                            <span class="name"><?php the_sub_field('name'); ?></span>
                            <span class="city"><?php the_sub_field('city'); ?></span>
                        </div>
                    </div>

            	<?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>


    <?php if(have_rows('injured_reserve')): ?>
        <div class="injured-reserve">
            <h4>Injured Reserve</h4>

            <div class="grid">
                <?php while(have_rows('injured_reserve')): the_row(); ?>
             
                    <div class="player">
                        <div class="photo">
                            <img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>

                        <div class="info">
                            <span class="name"><?php the_sub_field('name'); ?></span>
                            <span class="city"><?php the_sub_field('city'); ?></span>
                        </div>
                    </div>

                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>


    <?php if(have_rows('staff')): ?>
        <div class="staff">
            <h4>Staff</h4>

            <div class="grid">
                <?php while(have_rows('staff')): the_row(); ?>
             
                    <div class="player">
                        <div class="photo">
                            <img src="<?php $image = get_sub_field('photo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>

                        <div class="info">
                            <span class="name"><?php the_sub_field('name'); ?></span>
                            <span class="city"><?php the_sub_field('city'); ?></span>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>
</div>