<article>
	<div class="image">
		<a href="<?php the_permalink(); ?>">
			<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="info">
		<h4><?php the_time('m/d/Y'); ?></h4>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php the_field('teaser'); ?>
	</div>
</article>