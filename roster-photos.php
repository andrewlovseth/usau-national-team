<?php

/*

	Template Name: Roster Photos
	Template Post Type: page, world_games, wugc, u24, wjuc, wcbu

*/

get_header(); ?>

  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<?php $posts = get_field('roster'); if( $posts ): ?>

				<section id="roster" class="roster-segment">

					<h2>Roster</h2>

					<div class="player-list">

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

					    	<?php get_template_part('partials/player-photo'); ?>

					    <?php endforeach; ?>

					</div>

				</section>

			<?php wp_reset_postdata(); endif; ?>


			<?php $posts = get_field('alternates'); if( $posts ): ?>

				<section id="alternates" class="roster-segment">

					<h2>Alternates</h2>

					<div class="player-list">

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

					    	<?php get_template_part('partials/player-photo'); ?>

					    <?php endforeach; ?>

					</div>

				</section>

			<?php wp_reset_postdata(); endif; ?>


			<?php $posts = get_field('staff'); if( $posts ): ?>

				<section id="staff" class="roster-segment">

					<h2>Staff</h2>

					<div class="player-list">

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

					    	<?php get_template_part('partials/player-photo'); ?>

					    <?php endforeach; ?>

					</div>

				</section>

			<?php wp_reset_postdata(); endif; ?>


		</div>
	</section>




<?php get_footer(); ?>