<!DOCTYPE html>
<html>
<head>

	<title><?php wp_title(); ?></title>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,700i|Rajdhani:400,700|Merriweather:400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<section id="utility-nav">
		<div class="wrapper">

			<a href="<?php echo site_url('/news/'); ?>" class="news">News</a>

			<?php if(have_rows('utility_nav', 'options')): while(have_rows('utility_nav', 'options')): the_row(); ?>
			 
			    <a href="<?php the_sub_field('link'); ?>">
			        <?php the_sub_field('label'); ?>
			    </a>

			<?php endwhile; endif; ?>


		</div>
	</section>


	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

				<h1><a href="<?php echo site_url('/'); ?>">National Team</a></h1>

			</div>

			<?php if(get_field('show_watch_live', 'options') == true): ?>
				<a href="<?php echo site_url('/watch-live'); ?>" class="watch-live">Watch Live</a>
			<?php endif; ?>

			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

		</div>
	</header>

	<?php get_template_part('partials/nav-bar'); ?>

	<?php get_template_part('partials/nav-dropdown'); ?>